# coding=utf-8
"""
Daniel Calderon, CC3501, 2019-1
Drawing a car via a scene graph
"""

import glfw
from OpenGL.GL import *
import OpenGL.GL.shaders
import numpy as np
import transformations as tr
import scene_graph_1b as sg
import sys
import time
import random



# We will use 32 bits data, so an integer has 4 bytes
# 1 byte = 8 bits
INT_BYTES = 4
rp = 0.5 / 7 # Razon proporcion

def basefish(r, g, b):

    # Here the new shape will be stored
    gpuShape = sg.GPUShape()

    # Defining locations and colors for each vertex of the shape
    vertexData = np.array([
    #   positions        colors
        -7.0 * rp,  1.0 * 1.5 * rp, 0.0,  r, g, b,
        -6.0 * rp,  1.0 * 1.5 * rp, 0.0,  r, g, b,
        -4.0 * rp,  0.0 * 1.5 * rp, 0.0,  r, g, b,
        -1.0 * rp,  2.0 * 1.5 * rp, 0.0,  r, g, b,
         3.0 * rp,  2.0 * 1.5 * rp, 0.0,  r, g, b,
         5.0 * rp,  0.0 * 1.5 * rp, 0.0,  r, g, b,
         5.0 * rp,  0.0 * 1.5 * rp, 0.0,  r, g, b,
         3.0 * rp, -2.0 * 1.5 * rp, 0.0,  r, g, b,
        -1.0 * rp, -2.0 * 1.5 * rp, 0.0,  r, g, b,
        -6.0 * rp, -1.4 * 1.5 * rp, 0.0,  r, g, b,
        -7.0 * rp, -1.5 * 1.5 * rp, 0.0,  r, g, b,
        -6.0 * rp,  0.0 * 1.5 * rp, 0.0,  r, g, b,
    # It is important to use 32 bits data
        ], dtype = np.float32)

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = np.array(
        [ 0, 1, 11,
         11, 9, 10,
          1, 2, 9,
          2, 3, 8,
          3, 7, 8,
          3, 4, 7,
          4, 6, 7,
          4, 6, 5], dtype= np.uint32)

    gpuShape.size = len(indices)

    # VAO, VBO and EBO and  for the shape
    gpuShape.vao = glGenVertexArrays(1)
    gpuShape.vbo = glGenBuffers(1)
    gpuShape.ebo = glGenBuffers(1)

    # Vertex data must be attached to a Vertex Buffer Object (VBO)
    glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo)
    glBufferData(GL_ARRAY_BUFFER, len(vertexData) * INT_BYTES, vertexData, GL_STATIC_DRAW)

    # Connections among vertices are stored in the Elements Buffer Object (EBO)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(indices) * INT_BYTES, indices, GL_STATIC_DRAW)

    return gpuShape

def createQuad(r, g, b, k1=1, k2=1, k3=1, k4=1):

    # Here the new shape will be stored
    gpuShape = sg.GPUShape()

    # Defining locations and colors for each vertex of the shape    
    vertexData = np.array([
    #   positions        colors
        -0.5 * k1, -0.5, 0.0,  r, g, b,
         0.5, -0.5 * k2, 0.0,  r, g, b,
         0.5 * k3,  0.5, 0.0,  r, g, b,
        -0.5,  0.5 * k4, 0.0,  r, g, b
    # It is important to use 32 bits data
        ], dtype = np.float32)

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = np.array(
        [0, 1, 2,
         2, 3, 0], dtype= np.uint32)

    gpuShape.size = len(indices)

    # VAO, VBO and EBO and  for the shape
    gpuShape.vao = glGenVertexArrays(1)
    gpuShape.vbo = glGenBuffers(1)
    gpuShape.ebo = glGenBuffers(1)

    # Vertex data must be attached to a Vertex Buffer Object (VBO)
    glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo)
    glBufferData(GL_ARRAY_BUFFER, len(vertexData) * INT_BYTES, vertexData, GL_STATIC_DRAW)

    # Connections among vertices are stored in the Elements Buffer Object (EBO)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(indices) * INT_BYTES, indices, GL_STATIC_DRAW)

    return gpuShape

def createTrig(r, g, b, k1=1, k2=1, k3=1):

    # Here the new shape will be stored
    gpuShape = sg.GPUShape()

    # Defining locations and colors for each vertex of the shape
    vertexData = np.array([
    #   positions        colors
        -0.5 * k1,  0.0     , 0.0,  r, g, b,
         0.5 * k2,  0.0     , 0.0,  r, g, b,
         0.0     ,  0.5 * k3, 0.0,  r, g, b,
    # It is important to use 32 bits data
        ], dtype = np.float32)

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = np.array(
        [0, 1, 2
                ], dtype= np.uint32)

    gpuShape.size = len(indices)

    # VAO, VBO and EBO and  for the shape
    gpuShape.vao = glGenVertexArrays(1)
    gpuShape.vbo = glGenBuffers(1)
    gpuShape.ebo = glGenBuffers(1)

    # Vertex data must be attached to a Vertex Buffer Object (VBO)
    glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo)
    glBufferData(GL_ARRAY_BUFFER, len(vertexData) * INT_BYTES, vertexData, GL_STATIC_DRAW)

    # Connections among vertices are stored in the Elements Buffer Object (EBO)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(indices) * INT_BYTES, indices, GL_STATIC_DRAW)

    return gpuShape


def createHojaAlga(r, g, b):


    # Here the new shape will be stored
    gpuShape = sg.GPUShape()
    rp2 = 0.5/8.0
    # Defining locations and colors for each vertex of the shape
    vertexData = np.array([
    #   positions        colors
        -1.0 * rp2, -8.0 * rp2, 0, r, g, b,
         1.0 * rp2, -8.0 * rp2, 0, r, g, b,
        -3.0 * rp2, -5.0 * rp2, 0, r, g, b,
        -1.0 * rp2, -5.0 * rp2, 0, r, g, b,
        -1.0 * rp2,  0.0 * rp2, 0, r, g, b,
         1.0 * rp2,  0.0 * rp2, 0, r, g, b,
        -2.0 * rp2,  4.0 * rp2, 0, r, g, b,
        -1.0 * rp2,  4.0 * rp2, 0, r, g, b,
        -2.0 * rp2,  5.0 * rp2, 0, r, g, b,
        -1.0 * rp2,  5.0 * rp2, 0, r, g, b,
         0.0 * rp2,  8.0 * rp2, 0, r, g, b
    # It is important to use 32 bits data
        ], dtype = np.float32)

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = np.array(
        [0, 1, 2,
         1, 2, 3,
         2, 3, 4,
         3, 4, 5,
         4, 5, 6,
         5, 6, 7,
         6, 7, 8,
         7, 8, 9,
         8, 9, 10], dtype= np.uint32)

    gpuShape.size = len(indices)

    # VAO, VBO and EBO and  for the shape
    gpuShape.vao = glGenVertexArrays(1)
    gpuShape.vbo = glGenBuffers(1)
    gpuShape.ebo = glGenBuffers(1)

    # Vertex data must be attached to a Vertex Buffer Object (VBO)
    glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo)
    glBufferData(GL_ARRAY_BUFFER, len(vertexData) * INT_BYTES, vertexData, GL_STATIC_DRAW)

    # Connections among vertices are stored in the Elements Buffer Object (EBO)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(indices) * INT_BYTES, indices, GL_STATIC_DRAW)

    return gpuShape

def createRoca():


    # Here the new shape will be stored
    gpuShape = sg.GPUShape()
    # Defining locations and colors for each vertex of the shape
    vertexData = np.array([
    #   positions        colors

        1.0, 4.0, 0, 255/255, 102/255, 102/255,
        3.0, 2.0, 0, 0.5, 0.5, 0.5,
        3.0, 1.0, 0, 0.5, 0.5, 0.5,
        4.0, 0.0, 0, 0, 0, 0,
       -6.0, 0.0, 0, 0, 0, 0,
       -6.0, 1.0, 0, 0.5, 0.5, 0.5,
       -4.0, 2.0, 0, 0.5, 0.5, 0.5,
       -3.0, 3.0, 0, 0.75, 0.75, 0.75,
       -2.0, 3.0, 0, 0.75, 0.75, 0.75,
       -1.0, 4.0, 0, 255/255, 102/255, 102/255

    # It is important to use 32 bits data
        ], dtype = np.float32)

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = np.array(
        [0, 1, 2,
         0, 2, 9,
         9, 2, 8,
         8, 2, 4,
         4, 3, 2,
         4, 8, 6,
         7, 8, 6,
         5, 6, 4], dtype= np.uint32)

    gpuShape.size = len(indices)

    # VAO, VBO and EBO and  for the shape
    gpuShape.vao = glGenVertexArrays(1)
    gpuShape.vbo = glGenBuffers(1)
    gpuShape.ebo = glGenBuffers(1)

    # Vertex data must be attached to a Vertex Buffer Object (VBO)
    glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo)
    glBufferData(GL_ARRAY_BUFFER, len(vertexData) * INT_BYTES, vertexData, GL_STATIC_DRAW)

    # Connections among vertices are stored in the Elements Buffer Object (EBO)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(indices) * INT_BYTES, indices, GL_STATIC_DRAW)

    return gpuShape

def createArena():

    # Here the new shape will be stored
    gpuShape = sg.GPUShape()

    rx = 1/8
    ry = 1/5
    # Defining locations and colors for each vertex of the shape
    vertexData = np.array([
    #   positions        colors
        -8.0 * rx, 1.0 * ry, 0, 255/255, 153/255, 51/255,
        -6.0 * rx, 2.0 * ry, 0, 255/255, 128/255, 0,
        -4.0 * rx, 1.0 * ry, 0, 204/255, 102/255, 0,
        -1.0 * rx, 2.0 * ry, 0, 255/255, 153/255, 51/255,
         0.0 * rx, 1.0 * ry, 0, 153/255, 73/255, 0,
         2.0 * rx, 2.0 * ry, 0, 102/255, 51/255, 0,
         5.0 * rx, 1.0 * ry, 0, 204/255, 102/255, 0,
         6.0 * rx, 2.0 * ry, 0, 255/255, 153/255, 51/255,
         8.0 * rx, 2.0 * ry, 0, 255/255, 128/255, 0,
         5.0 * rx, 1.0 * ry, 0, 102/255, 51/255, 0,
         3.0 * rx, 0.0 * ry, 0, 153/255, 73/255, 0,
        -1.0 * rx, 0.0 * ry, 0, 204/255, 102/255, 0,
        -3.0 * rx, 0.0 * ry, 0, 153/255, 73/255, 0,
        -6.0 * rx, 1.0 * ry, 0, 153/255, 73/255, 0,
        -8.0 * rx, 0.0 * ry, 0, 153/255, 73/255, 0,
         8.0 * rx, 0.0 * ry, 0, 204/255, 102/255, 0,
        # It is important to use 32 bits data
        ], dtype = np.float32)

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = np.array(
        [0, 1, 2,
         2, 3, 4,
         4, 5, 6,
         6, 7, 8,
         8, 10, 9,
         11, 4, 10,
         11, 12, 2,
         12, 13, 2,
         14, 13, 0,
         15, 8, 9,
         10, 9, 15,
         11, 10, 6,
         11, 4, 2,
         14, 13, 12,
         4, 6, 10], dtype= np.uint32)

    gpuShape.size = len(indices)

    # VAO, VBO and EBO and  for the shape
    gpuShape.vao = glGenVertexArrays(1)
    gpuShape.vbo = glGenBuffers(1)
    gpuShape.ebo = glGenBuffers(1)

    # Vertex data must be attached to a Vertex Buffer Object (VBO)
    glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo)
    glBufferData(GL_ARRAY_BUFFER, len(vertexData) * INT_BYTES, vertexData, GL_STATIC_DRAW)

    # Connections among vertices are stored in the Elements Buffer Object (EBO)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(indices) * INT_BYTES, indices, GL_STATIC_DRAW)

    return gpuShape

def createAncla():

    # Here the new shape will be stored
    gpuShape = sg.GPUShape()
    rp2 = 0.5 / 8.0
    # Defining locations and colors for each vertex of the shape
    vertexData = np.array([
        #   positions        colors
        1.0,  8.0, 0, 0.3, 0.3, 0.3,
        2.0,  7.0, 0, 0, 0, 0,
        2.0,  6.0, 0, 0, 0, 0,
        1.0,  5.0, 0, 0, 0, 0,
        1.0, -2.0, 0, 0, 0, 0,
        2.0, -3.0, 0, 0, 0, 0,
        3.0, -3.0, 0, 0, 0, 0,
        4.0, -2.0, 0, 0, 0, 0,
        4.0, -1.0, 0, 0.3, 0.3, 0.3,
        5.0, -2.0, 0, 0, 0, 0,
        4.0, -4.0, 0, 0, 0, 0,
        2.0, -5.0, 0, 0, 0, 0,
        0.0, -5.0, 0, 0, 0, 0,
       -2.0, -5.0, 0, 0, 0, 0,
       -4.0, -4.0, 0, 0, 0, 0,
       -5.0, -2.0, 0, 0, 0, 0,
       -4.0, -1.0, 0, 0.3, 0.3, 0.3,
       -4.0, -2.0, 0, 0, 0, 0,
       -3.0, -3.0, 0, 0, 0, 0,
       -2.0, -3.0, 0, 0, 0, 0,
       -1.0, -2.0, 0, 0, 0, 0,
       -1.0,  5.0, 0, 0, 0, 0,
       -2.0,  6.0, 0, 0, 0, 0,
       -2.0,  7.0, 0, 0, 0, 0,
       -1.0, 8.0, 0, 0.3, 0.3, 0.3,

        # It is important to use 32 bits data
    ], dtype=np.float32)

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = np.array(
        [0, 1, 2,
         2, 3, 0,
         0, 3, 24,
         3, 21, 24,
         24, 21, 23,
         21, 23, 22,
         21, 3, 20,
         20, 4, 3,
         20, 4, 5,
         20, 5, 19,
         7, 8, 9,
         7, 9, 10,
         6, 7, 10,
         5, 6, 10,
         5, 10, 11,
         5, 11, 12,
         5, 19, 12,
         12, 13, 19,
         19, 14, 18,
         14, 18, 17,
         14,15,16,
         14, 19, 13], dtype=np.uint32)

    gpuShape.size = len(indices)

    # VAO, VBO and EBO and  for the shape
    gpuShape.vao = glGenVertexArrays(1)
    gpuShape.vbo = glGenBuffers(1)
    gpuShape.ebo = glGenBuffers(1)

    # Vertex data must be attached to a Vertex Buffer Object (VBO)
    glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo)
    glBufferData(GL_ARRAY_BUFFER, len(vertexData) * INT_BYTES, vertexData, GL_STATIC_DRAW)

    # Connections among vertices are stored in the Elements Buffer Object (EBO)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(indices) * INT_BYTES, indices, GL_STATIC_DRAW)

    return gpuShape

def createFondo():

    # Here the new shape will be stored
    gpuShape = sg.GPUShape()
    rp2 = 0.5 / 8.0
    # Defining locations and colors for each vertex of the shape
    vertexData = np.array([
        #   positions        colors
            -1.0, -1.0, 0, 0, 153/255, 153/255,
             1.0, -1.0, 0, 0, 153/255, 183/255,
             1.0,  1.0, 0, 150/255, 200/255, 255/255,
            -1.0,  1.0, 0, 150/255, 200/255, 255/255,
        # It is important to use 32 bits data
    ], dtype=np.float32)

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = np.array(
        [0, 1, 2,
         2, 3, 0], dtype=np.uint32)

    gpuShape.size = len(indices)

    # VAO, VBO and EBO and  for the shape
    gpuShape.vao = glGenVertexArrays(1)
    gpuShape.vbo = glGenBuffers(1)
    gpuShape.ebo = glGenBuffers(1)

    # Vertex data must be attached to a Vertex Buffer Object (VBO)
    glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo)
    glBufferData(GL_ARRAY_BUFFER, len(vertexData) * INT_BYTES, vertexData, GL_STATIC_DRAW)

    # Connections among vertices are stored in the Elements Buffer Object (EBO)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(indices) * INT_BYTES, indices, GL_STATIC_DRAW)

    return gpuShape

def createFish1():

    # Main fish body
    base = sg.SceneGraphNode("base")
    base.childs += [basefish(1, 0, 0)]

    # Agallas
    agallas_unit = sg.SceneGraphNode("agallas")
    #agallas_unit.transform = tr.rotationZ(30 * np.pi/180)
    #agallas_unit.transform = tr.shearing(0.1, 0.2, 0.3, 0.4, 0.5, 0.6)
    agallas_unit.transform = tr.uniformScale(0.1)
    agallas_unit.childs += [createTrig(100/255, 170/255, 150/255, 1, 1, 1)]

    agallas_rot = sg.SceneGraphNode("agrot")
    agallas_rot.transform = tr.rotationZ(30 * np.pi/180)
    agallas_rot.childs += [agallas_unit]

    frontagalla = sg.SceneGraphNode("agfront")
    frontagalla.transform = tr.translate(1 * rp, 1.5 * rp, 0)
    frontagalla.childs += [agallas_rot]

    backagalla = sg.SceneGraphNode("agback")
    backagalla.transform = tr.translate(1 * rp, -1.2 * rp, 0)
    backagalla.childs += [agallas_rot]

    midagalla = sg.SceneGraphNode("midagalla")
    midagalla.transform = tr.translate(-0.5 * rp, 0 , 0)
    midagalla.childs += [agallas_rot]

    agallas = sg.SceneGraphNode("agallas")
    agallas.transform = tr.translate(-0.5 * rp, 0, 0)
    agallas.childs += [midagalla]
    agallas.childs += [frontagalla]
    agallas.childs += [backagalla]

    fon_ojo = sg.SceneGraphNode("fonojo")
    fon_ojo.childs += [createQuad(1, 1, 1)]

    pupila = sg.SceneGraphNode("pupila")
    pupila.transform = tr.uniformScale(0.5)
    pupila.childs += [createQuad(0, 0, 0)]

    ojo = sg.SceneGraphNode("ojo")
    ojo.transform = tr.uniformScale(0.05)
    ojo.childs += [fon_ojo]
    ojo.childs += [pupila]

    ojo_rot = sg.SceneGraphNode("ojo_rot")
    ojo_rot.transform = tr.rotationZ(20 * np.pi/180)
    ojo_rot.childs += [ojo]

    ojo_trans = sg.SceneGraphNode("ojo_trans")
    ojo_trans.transform = tr.translate(2.8 * rp, 1.5 * rp, 0)
    ojo_trans.childs += [ojo_rot]

    #Aletas
    aleta_inf_rot = sg.SceneGraphNode("aletainfrot")
    aleta_inf_rot.transform = tr.rotationZ(np.pi)
    aleta_inf_rot.childs += [createTrig(255/255, 153/255, 51/255, 1, 2, 1)]

    aleta_inf = sg.SceneGraphNode("aleta_inf")
    aleta_inf.transform = tr.translate(0.6, -1.77, 0)
    aleta_inf.childs += [aleta_inf_rot]

    aleta_sup = sg.SceneGraphNode("aleta_sup")
    aleta_sup.transform = tr.translate(0, 1.77, 0)
    aleta_sup.childs += [createTrig(255/255, 153/255, 51/255, 1, 3, 1)]

    aletas = sg.SceneGraphNode("aletas")
    aletas.transform = tr.uniformScale(0.12)
    aletas.childs += [aleta_inf]
    aletas.childs += [aleta_sup]



    # Complete Fish
    fish = sg.SceneGraphNode("fish")
    fish.transform = tr.uniformScale(0.5)
    fish.childs += [base]
    fish.childs += [agallas]
    fish.childs += [ojo_trans]
    fish.childs += [aletas]

    FinalFish = sg.SceneGraphNode("FinalFish")
    FinalFish.childs += [fish]
    return FinalFish

def createFish2():

    # Main fish body
    base = sg.SceneGraphNode("base")
    base.childs += [basefish(1, 0, 1)]

    # Agallas
    agallas_unit = sg.SceneGraphNode("agallas")
    agallas_unit.transform = tr.matmul([tr.uniformScale(0.05), tr.shearing(0.1, 0.3, 0.1, 0.4, 0.6, 0.7)])
    agallas_unit.childs += [createQuad(100/255, 170/255, 150/255, 1, 1, 1)]

    agallas_rot = sg.SceneGraphNode("agrot")
    agallas_rot.transform = tr.rotationZ(30 * np.pi/180)
    agallas_rot.childs += [agallas_unit]

    frontagalla = sg.SceneGraphNode("agfront")
    frontagalla.transform = tr.translate(1 * rp, 1.5 * rp, 0)
    frontagalla.childs += [agallas_rot]

    backagalla = sg.SceneGraphNode("agback")
    backagalla.transform = tr.translate(1 * rp, -1.2 * rp, 0)
    backagalla.childs += [agallas_rot]

    midagalla = sg.SceneGraphNode("midagalla")
    midagalla.transform = tr.translate(-0.5 * rp, 0 , 0)
    midagalla.childs += [agallas_rot]

    fourthagalla = sg.SceneGraphNode("fourthagalla")
    fourthagalla.transform = tr. translate(-1.5 * rp, 0.5 * rp, 0)
    fourthagalla.childs += [agallas_rot]

    agallas = sg.SceneGraphNode("agallas")
    agallas.transform = tr.translate(-0.5 * rp, 0, 0)
    agallas.childs += [midagalla]
    agallas.childs += [frontagalla]
    agallas.childs += [backagalla, fourthagalla]

    fon_ojo = sg.SceneGraphNode("fonojo")
    fon_ojo.childs += [createQuad(1, 1, 1)]

    pupila = sg.SceneGraphNode("pupila")
    pupila.transform = tr.uniformScale(0.5)
    pupila.childs += [createQuad(0, 0, 0)]

    ojo = sg.SceneGraphNode("ojo")
    ojo.transform = tr.uniformScale(0.05)
    ojo.childs += [fon_ojo]
    ojo.childs += [pupila]

    ojo_rot = sg.SceneGraphNode("ojo_rot")
    #ojo_rot.transform = tr.rotationZ(20 * np.pi/180)
    ojo_rot.childs += [ojo]

    ojo_trans = sg.SceneGraphNode("ojo_trans")
    ojo_trans.transform = tr.translate(2.8 * rp, 1.5 * rp, 0)
    ojo_trans.childs += [ojo_rot]

    #Aletas
    aleta_inf_rot = sg.SceneGraphNode("aletainfrot")
    aleta_inf_rot.transform = tr.rotationZ(np.pi)
    aleta_inf_rot.childs += [createTrig(1, 0.5, 0.5, 2, -1, 2)]

    aleta_inf = sg.SceneGraphNode("aleta_inf")
    aleta_inf.transform = tr.translate(0, -1.77, 0)
    aleta_inf.childs += [aleta_inf_rot]

    aleta_sup = sg.SceneGraphNode("aleta_sup")
    aleta_sup.transform = tr.translate(0, 1.77, 0)
    aleta_sup.childs += [createTrig(1, 0.5, 0.5, -0.5, 3, 3)]

    aletas = sg.SceneGraphNode("aletas")
    aletas.transform = tr.uniformScale(0.12)
    aletas.childs += [aleta_inf]
    aletas.childs += [aleta_sup]



    # Complete Fish
    fish = sg.SceneGraphNode("fish")
    fish.transform = tr.matmul([tr.scale(0.8 , 0.5, 0),tr.uniformScale(0.5)])
    fish.childs += [base]
    fish.childs += [agallas]
    fish.childs += [ojo_trans]
    fish.childs += [aletas]

    FinalFish = sg.SceneGraphNode("FinalFish")
    FinalFish.childs += [fish]
    return FinalFish


def createFish3():

    # Main fish body
    base = sg.SceneGraphNode("base")
    base.childs += [basefish(51/255, 0, 51/255)]

    # Agallas (En realidad son escamas de colores xD)
    agallas_unit = sg.SceneGraphNode("agallas")
    #agallas_unit.transform = tr.rotationZ(30 * np.pi/180)
    #agallas_unit.transform = tr.shearing(0.1, 0.2, 0.3, 0.4, 0.5, 0.6)
    agallas_unit.transform = tr.uniformScale(0.02)
    agallas_unit.childs += [createQuad(0,1,0,2,3,4,6)]

    agallas_rot = sg.SceneGraphNode("agrot")
    agallas_rot.transform = tr.rotationZ(30 * np.pi/180)
    agallas_rot.childs += [agallas_unit]

    frontagalla = sg.SceneGraphNode("agfront")
    frontagalla.transform = tr.translate(1 * rp, 1.5 * rp, 0)
    frontagalla.childs += [agallas_rot]

    backagalla = sg.SceneGraphNode("agback")
    backagalla.transform = tr.translate(1 * rp, -1.2 * rp, 0)
    backagalla.childs += [agallas_rot]

    midagalla = sg.SceneGraphNode("midagalla")
    midagalla.transform = tr.translate(-0.5 * rp, 0 , 0)
    midagalla.childs += [agallas_rot]

    fourthagalla = sg.SceneGraphNode("fourthagalla")
    fourthagalla.transform = tr.translate(2 * rp, 0, 0)
    fourthagalla.childs += [agallas_rot]

    agallas = sg.SceneGraphNode("agallas")
    agallas.transform = tr.translate(-0.8 * rp, 0, 0)
    agallas.childs += [midagalla]
    agallas.childs += [frontagalla]
    agallas.childs += [backagalla]
    agallas.childs += [fourthagalla]

    fon_ojo = sg.SceneGraphNode("fonojo")
    fon_ojo.childs += [createQuad(1, 1, 1)]

    pupila = sg.SceneGraphNode("pupila")
    pupila.transform = tr.uniformScale(0.5)
    pupila.childs += [createQuad(0, 0, 0)]

    ojo = sg.SceneGraphNode("ojo")
    ojo.transform = tr.uniformScale(0.05)
    ojo.childs += [fon_ojo]
    ojo.childs += [pupila]

    ojo_rot = sg.SceneGraphNode("ojo_rot")
    ojo_rot.transform = tr.rotationZ(20 * np.pi/180)
    ojo_rot.childs += [ojo]

    ojo_trans = sg.SceneGraphNode("ojo_trans")
    ojo_trans.transform = tr.translate(2.8 * rp, 1.5 * rp, 0)
    ojo_trans.childs += [ojo_rot]

    #Aletas
    c1= 0.2

    aleta_inf_rot = sg.SceneGraphNode("aletainfrot")
    aleta_inf_rot.transform = tr.matmul([tr.rotationZ(np.pi), tr.scale(0.3, 0.6, 1), tr.rotationY(np.pi)])
    aleta_inf_rot.childs += [createQuad(1, 1, 51/255, 1, 1, -0.3, -0.4)]

    aleta_inf = sg.SceneGraphNode("aleta_inf")
    aleta_inf.transform = tr.translate(0.9 + c1, -2.05, 0)
    aleta_inf.childs += [aleta_inf_rot]

    aleta_inf2 = sg.SceneGraphNode("aleta_inf2")
    aleta_inf2.transform = tr.translate(-0.3 + c1, -2.05, 0)
    aleta_inf2.childs += [aleta_inf_rot]

    aleta_inf3 = sg.SceneGraphNode("aleta_inf3")
    aleta_inf3.transform = tr.translate(0.1+ c1, -2.05, 0)
    aleta_inf3.childs += [aleta_inf_rot]

    aleta_inf4 = sg.SceneGraphNode("aleta_inf4")
    aleta_inf4.transform = tr.translate(0.5+ c1, -2.05, 0)
    aleta_inf4.childs += [aleta_inf_rot]

    aleta_sup = sg.SceneGraphNode("aleta_sup")
    aleta_sup.transform = tr.matmul([tr.translate(0, 2.27, 0), tr.rotationY(0)])
    aleta_sup.childs += [createQuad(1, 1, 51/255, 1, 1, -0.2, -0.1)]

    aleta_sup2 = sg.SceneGraphNode("aleta_sup2")
    aleta_sup2.transform = tr.matmul([tr.translate(1, 2.27, 0), tr.rotationY(0)])
    aleta_sup2.childs += [createQuad(1, 1, 51/255, 1, 1, -0.2, -0.1)]

    aletas = sg.SceneGraphNode("aletas")
    aletas.transform = tr.uniformScale(0.12)
    aletas.childs += [aleta_inf, aleta_inf2, aleta_inf3, aleta_inf4]
    aletas.childs += [aleta_sup, aleta_sup2]



    # Complete Fish
    fish = sg.SceneGraphNode("fish")
    fish.transform = tr.matmul([tr.scale(0.5,0.7,1),tr.uniformScale(0.5)])
    fish.childs += [base]
    fish.childs += [agallas]
    fish.childs += [ojo_trans]
    fish.childs += [aletas]

    FinalFish = sg.SceneGraphNode("FinalFish")
    FinalFish.childs += [fish]
    return FinalFish


def createRoquerio():

    roca1 = sg.SceneGraphNode("roca1")
    roca1.transform = tr.matmul([tr.uniformScale(0.2*1/5)])
    roca1.childs += [createRoca()]

    roca2 = sg.SceneGraphNode("roca2")
    roca2.transform = tr.matmul([tr.uniformScale(0.15*1/5), tr.translate(-0.1,0,0)])
    roca2.childs += [createRoca()]

    roca3 = sg.SceneGraphNode("roca3")
    roca3.transform = tr.matmul([tr.uniformScale(0.05*1/5), tr.translate(0.15,-0.05,0)])
    roca3.childs += [createRoca()]

    roca4 = sg.SceneGraphNode("roca4")
    roca4.transform = tr.matmul([tr.uniformScale(0.1*1/5), tr.translate(-0.2,-0.05,0)])
    roca4.childs += [createRoca()]

    roquerio = sg.SceneGraphNode("roquerio")
    roquerio.transform = tr.translate(0, -0.9 ,0)
    roquerio.childs += [roca1,roca2, roca3, roca4]

    return roquerio

def createAlga():

    hoja1 = sg.SceneGraphNode("hoja1")
    hoja1.childs += [createHojaAlga(0, 153/255, 0)]

    hoja2 = sg.SceneGraphNode("hoja2")
    hoja2.transform = tr.matmul([tr.uniformScale(0.5), tr.rotationY(np.pi), tr.translate(0, -0.25, 0)])
    hoja2.childs += [createHojaAlga(102/255, 204/255, 0)]

    hoja3 = sg.SceneGraphNode("hoja3")
    hoja3.transform = tr.matmul([tr.scale(0.5, 1.2, 0), tr.rotationY(np.pi), tr.translate(0.1, 0.1, 0)])
    hoja3.childs += [createHojaAlga(76/255, 156/255, 0)]

    hoja4 = sg.SceneGraphNode("hoja1")
    hoja4.transform = tr.matmul([tr. rotationY(np.pi), tr.translate(0.2, 0, 0), tr.scale(0.7, 1, 1)])
    hoja4.childs += [createHojaAlga(0, 102/255, 0)]

    hoja5 = sg.SceneGraphNode("hoja1")
    hoja5.transform = tr.matmul([tr.scale(1.2,1.5,1), tr.translate(0.13,0.25,0)])
    hoja5.childs += [createHojaAlga(0, 51/255, 25/255)]

    alga = sg.SceneGraphNode("alga")
    alga.childs += [hoja5, hoja1, hoja2, hoja3, hoja4]

    return alga



def createCar():
    
    # Cheating a single wheel
    wheel = sg.SceneGraphNode("wheel")
    wheel.transform = tr.uniformScale(0.2)
    wheel.childs += [createQuad(0,0,0)]

    wheelRotation = sg.SceneGraphNode("wheelRotation")
    wheelRotation.childs += [wheel]

    # Instanciating 2 wheels, for the front and back parts
    frontWheel = sg.SceneGraphNode("frontWheel")
    frontWheel.transform = tr.translate(0.3,-0.3,0)
    frontWheel.childs += [wheelRotation]

    backWheel = sg.SceneGraphNode("backWheel")
    backWheel.transform = tr.translate(-0.3,-0.3,0)
    backWheel.childs += [wheelRotation]
    
    # Creating the chasis of the car
    chasis = sg.SceneGraphNode("chasis")
    chasis.transform = tr.scale(1,0.5,1)
    chasis.childs += [basefish(1,0,0)]

    car = sg.SceneGraphNode("car")
    car.childs += [chasis]
    car.childs += [frontWheel]
    car.childs += [backWheel]

    traslatedCar = sg.SceneGraphNode("traslatedCar")
    traslatedCar.transform = tr.translate(0,0.3,0)
    traslatedCar.childs += [car]

    return traslatedCar

def createScene():

    fondo = sg.SceneGraphNode("fondo")
    fondo.childs += [createFondo()]

    arena = sg.SceneGraphNode("arena")
    arena.transform = tr.translate(0, -1, 0)
    arena.childs += [createArena()]

    ancla = sg.SceneGraphNode("ancla")
    ancla.transform = tr.matmul([tr.uniformScale((0.5/8)*0.5), tr.rotationZ(-np.pi/8), tr.translate(-0.5,-0.7,0)])
    ancla.childs += [createAncla()]

    alga1 = sg.SceneGraphNode("alga1")
    alga1.transform = tr.matmul([tr.uniformScale(0.5), tr.translate(-0.8, -0.55, 0)])
    alga1.childs += [createAlga()]

    alga2 = sg.SceneGraphNode("alga2")
    alga2.transform = tr.matmul([tr.uniformScale(0.2), tr.translate(-0.65, -0.7, 0)])
    alga2.childs += [createAlga()]

    alga3 = sg.SceneGraphNode("alga3")
    alga3.transform = tr.matmul([tr.uniformScale(0.3), tr.translate(-0.9, -0.65, 0)])
    alga3.childs += [createAlga()]

    alga4 = sg.SceneGraphNode("alga4")
    alga4.transform = tr.matmul([tr.scale(0.4, 1, 0), tr.translate(0.8, -0.1, 0)])
    alga4.childs += [createAlga()]

    roquerio1 = sg.SceneGraphNode("roquerio1")
    roquerio1.childs+=[createRoquerio()]

    scene = sg.SceneGraphNode("scene")
    scene.childs += [fondo, arena, roquerio1, alga1, alga2, alga3, ancla, alga4]


    return scene


def movSinuside(px, py, alfa=0):
    if -np.sin(0.5 * glfw.get_time() + px) < 0:
         alfa = np.pi
    if -np.sin(0.5 * glfw.get_time() + px) > 0:
         alfa = 2 * np.pi
    dy = 0.8 * np.sin(0.5 * glfw.get_time() + py)
    dx = 0.8 * np.cos(0.5 * glfw.get_time() + px)
    trans = tr.matmul([tr.rotationY(alfa), tr.rotationZ(0.2*np.sin(4*glfw.get_time())), tr.translate(dx,dy,0)])
    return [trans, alfa]


def movElipse(px, py, alfa):
    if -np.sin(0.5 * glfw.get_time() + px) < 0:
        alfa = np.pi
    if -np.sin(0.5 * glfw.get_time() + px) > 0:
        alfa = 2 * np.pi
    dx = 0.7 * np.cos(0.5 * glfw.get_time() + px)
    dy =  0.9 * np.sin(0.5 * glfw.get_time() + px)
    trans = tr.matmul([tr.rotationY(alfa), tr.rotationZ(0.2*np.sin(4*glfw.get_time())), tr.translate(dx,dy,0)])
    return trans, alfa

def movParabola(px, py, alfa=0):
    if -np.sin(0.5 * glfw.get_time() + px) < 0:
        alfa = np.pi
    if -np.sin(0.5 * glfw.get_time() + px) > 0:
        alfa = 2 * np.pi
    dx = 0.7 * np.cos(0.5 * glfw.get_time() + px)
    dy = dx**2
    trans = tr.matmul([tr.rotationY(alfa), tr.rotationZ(0.2 * np.sin(4 * glfw.get_time())), tr.translate(dx, dy, 0)])
    return trans, alfa


def modscene(estado, scene, contador):
    if estado == 1:
        px = 0.8 * np.random.rand(1) - 0.8 * np.random.rand(1)
        py = 0.8 * np.random.rand(1) - 0.8 * np.random.rand(1)
        fish = [createFish1(), createFish2(), createFish3()]
        newfish = sg.SceneGraphNode("newfish"+str(contador))
        newfish.transform = tr.translate(px, py, 0)
        newfish.childs += [fish[int(np.random.randint(0,3,1))]]
        fishes = sg.findNode(scene, "fishes")
        fishes.childs += [newfish]
        return scene
    else:
        return scene

def borrarpez(scene, cursor_pos):
    fishes = sg.findNode(scene, "fishes")
    print(len(fishes.childs))
    tolerancia = 0.1
    for fish in fishes.childs:
        print(fish.name)
        fish_pos = sg.findPosition(fishes, fish.name, tr.identity())
        norm = np.linalg.norm(cursor_pos - fish_pos[0:2])
        print("norm = " + str(norm))
        if norm <= tolerancia:
            fish.childs = []


# A class to store the application control
class Controller:
    def _init_(self):
        self.estado = 0

    fillPolygon = True


# we will use the global controller as communication with the callback function
controller = Controller()
contador = 0

def on_key(window, key, scancode, action, mods):
    if action != glfw.PRESS:
        return

    global controller
    global contador
    if key == glfw.KEY_ESCAPE:
        sys.exit()

    if key == glfw.KEY_ENTER:
        contador += 1
        modscene(1, scene, contador)
    else:
        print('Unknown key')

def mouse_click(windows, button, action, mods):

    if button == glfw.MOUSE_BUTTON_LEFT and action == glfw.PRESS:
        cursor_pos_s0 = (np.array(glfw.get_cursor_pos(window)) * np.array([1/850, 1/850]))
        cursor_pos = [cursor_pos_s0[0] - 1, 1 - cursor_pos_s0[1]]
        print(cursor_pos)
        borrarpez(scene, cursor_pos)
    else:
        return

if __name__ == "__main__":

    # Initialize glfw
    if not glfw.init():
        sys.exit()

    width = 1700
    height = 1700


    window = glfw.create_window(width, height, "Drawing a Quad via a EBO", None, None)

    if not window:
        glfw.terminate()
        sys.exit()

    glfw.make_context_current(window)

    # Connecting the callback function 'on_key' to handle keyboard events
    glfw.set_mouse_button_callback(window, mouse_click)
    glfw.set_key_callback(window, on_key)

    # Assembling the shader program (pipeline) with both shaders
    shaderProgram = sg.basicShaderProgram()
    
    # Telling OpenGL to use our shader program
    glUseProgram(shaderProgram)

    # Setting up the clear screen color
    glClearColor(0.85, 0.85, 0.85, 1.0)

    # Creating shapes on GPU memory
    scene = createScene()
    # Posiciones random
    px = np.random.rand(5)
    py = np.random.rand(5)

    # 5 Peces iniciales
    fish1 = sg.SceneGraphNode("fish1")
    fish1.transform = tr.matmul([tr.translate(px[0], py[0], 0)])
    fish1.childs += [createFish1()]

    fish2 = sg.SceneGraphNode("fish2")
    fish2.transform = tr.matmul([tr.translate(px[1], py[1], 0)])
    fish2.childs += [createFish2()]

    fish3 = sg.SceneGraphNode("fish3")
    fish3.transform = tr.matmul([tr.uniformScale(1), tr.translate(px[2], py[2], 0)])
    fish3.childs += [createFish3()]

    fish4 = sg.SceneGraphNode("fish4")
    fish4.transform = tr.matmul([tr.uniformScale(1), tr.translate(px[3], py[3], 0)])
    fish4.childs += [createFish3()]

    fish5 = sg.SceneGraphNode("fish5")
    fish5.transform = tr.matmul([tr.translate(px[4], py[4], 0)])
    fish5.childs += [createFish2()]

    fishes = sg.SceneGraphNode("fishes")
    fishes.childs += [fish1, fish2, fish3, fish4]

    scene.childs += [fishes]

    # Our shapes here are always fully painted
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)

    #Parametros de inicializacion para la animacion
    dx = 0
    dy = 0
    alfa = 0
    p0x = []
    p0y = []
    fishes = sg.findNode(scene, "fishes")

    while not glfw.window_should_close(window):
        # # Using GLFW to check for input events
        glfw.poll_events()

        # Clearing the screen in both, color and depth
        glClear(GL_COLOR_BUFFER_BIT)

        # Animacion de la escena
        random.seed(108) #posiciones iniciales fijas
        for i in range(len(fishes.childs)):
            p0x.append(np.random.randint(0,10,1))
            p0y.append(np.random.randint(0,20,1))

        for k in range(len(fishes.childs)):
            fish = fishes.childs[k]
            animaciones = [tr.matmul([movParabola(p0x[k], p0y[k], alfa)[0]]),
                             tr.matmul([movElipse(p0x[k], p0y[k], alfa)[0]]),
                             tr.matmul([movSinuside(p0x[k], p0y[k], alfa)[0]])]
            fish.transform = animaciones[2]
            scene.transform = tr.matmul([tr.uniformScale(1.1),tr.shearing(0.02*np.sin(glfw.get_time()), 0, 0, 0, 0, 0)])



        # Drawing the Scene
        sg.drawSceneGraphNode(scene, shaderProgram, tr.identity())

        # Once the render is done, buffers are swapped, showing only the complete scene.
        glfw.swap_buffers(window)
    glfw.terminate()
